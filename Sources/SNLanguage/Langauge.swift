//
//  Langauge.swift
//  CopperTrade
//
//  Created by Saran Nonkamjan on 21/9/2562 BE.
//  Copyright © 2562 Saran Nonkamjan. All rights reserved.
//

import Foundation

public let UTLanguageChangeNotification = Notification.Name("UTLanguageChangeNotification")

public enum LangaugeType: Int {
    case th = 0
    case en
    case chinese

    public var localizedString: String {
        switch self {
        case .th:
            return "th"
        case .en:
            return "en"
        case .chinese:
            return "zh-Hans"
        }
    }
}

open class Langauge: NSObject {
    public static let sharedObject: Langauge = Langauge()

    override init() {
        self.langauge = LangaugeType(rawValue: UserDefaults.standard.integer(forKey: "langauge")) ?? LangaugeType.th
    }

    public class func localizedString(withKey key: String) -> String {
        return NSObject().l$(key)
    }

    public var langauge: LangaugeType {
        didSet {
            UserDefaults.standard.set(self.langauge.rawValue, forKey: "langauge")
            UserDefaults.standard.synchronize()

            NotificationCenter.default.post(name: UTLanguageChangeNotification, object: nil)
        }
    }
}

public extension NSObject {
    func l$(_ key: String) -> String {
        let bundle: Bundle = .main
        if let path = bundle.path(forResource: Langauge.sharedObject.langauge.localizedString, ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: key, value: nil, table: nil)
        }
        else if let path = bundle.path(forResource: "Base", ofType: "lproj"),
            let bundle = Bundle(path: path) {
            return bundle.localizedString(forKey: key, value: nil, table: nil)
        }
        return key
    }

    func k$(_ key: String) -> String {
        if Langauge.sharedObject.langauge == LangaugeType.th {
            return key
        }
        return key + "_" + Langauge.sharedObject.langauge.localizedString
    }
}
